<?php

include 'Ocean.php';

class Oceans
{
    private $oceans;

    public function __construct()
    {
        $this->oceans = [
            new Ocean('Тихий ', 178, 18),
            new Ocean('Атлантичний', 132, 9),
            new Ocean('Індійський', 78, 19),
            new Ocean('Південний', 100, 5),
        ];
    }

    public function getOceanCountByArea($area){
        $count = 0;
        foreach ($this->oceans as $ocean)
            if($ocean->getArea() >= $area)
                $count++;
        return $count;
    }

    public function getOceanByBestTemp(){
        $max = $this->oceans[0];
        foreach ($this->oceans as $ocean)
            if($ocean->getAverageTemp() > $max->getAverageTemp())
                $max = $ocean;
        return $max->getTitle();
    }
}