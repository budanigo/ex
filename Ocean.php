<?php


class Ocean
{
    private $title;
    private $area;
    private $average_temp;

    /**
     * Ocean constructor.
     * @param $title
     * @param $area
     * @param $average_temp
     */
    public function __construct($title, $area, $average_temp)
    {
        $this->title = $title;
        $this->area = $area;
        $this->average_temp = $average_temp;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return mixed
     */
    public function getAverageTemp()
    {
        return $this->average_temp;
    }

    /**
     * @param mixed $average_temp
     */
    public function setAverageTemp($average_temp)
    {
        $this->average_temp = $average_temp;
    }


}